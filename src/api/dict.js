import { axios } from '@/utils/request'

/*
 * 字典管理模块
 */

// 保存字典
export const addDict = (data) => {
  return axios({
    url: '/sys/dict',
    method: 'post',
    data: data
  })
}
// 获取字典列表
export const queryDictList = (parms) => {
  return axios({
    url: '/sys/dict',
    method: 'get',
    params: parms
  })
}

// 更新字典
export const updateDict = (data) => {
  return axios({
    url: '/sys/dict',
    method: 'put',
    data: data
  })
}
// 根据主键删除字典
export const deleteDict = (id) => {
  return axios({
    url: '/sys/dict/' + id,
    method: 'delete'
  })
}



// 根据字典id获取字典详情列表
export const queryDictItemList = (parms) => {
  return axios({
    url: '/sys/dictItem',
    method: 'get',
    params: parms
  })
}

// 根据id删除字典
export const deleteDictItem = (id) => {
  return axios({
    url: '/sys/dictItem/' + id,
    method: 'delete'
  })
}

// 保存字典详情
export const addDictItem = (data) => {
  return axios({
    url: '/sys/dictItem',
    method: 'post',
    data: data
  })
}
// 更新字典详情
export const updateDictItem = (data) => {
  return axios({
    url: '/sys/dictItem',
    method: 'put',
    data: data
  })
}